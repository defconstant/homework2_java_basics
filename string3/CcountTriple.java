/* We'll say that a "triple" in a string is a char appearing three times in a row. Return the
 * number of triples in the given string. The triples may overlap.
*/

package string3;

public class CcountTriple {
    public int countTriple(String str) {
	int count = 1, triple = 0;
	char prev = '\u0000';
	for (int i = 0; i < str.length(); ++i) {
	    if (str.charAt(i) == prev)
		++count;
	    else
		count = 1;
	    if (count >= 3)
		++triple;
	    prev = str.charAt(i);
	}
	return triple;
    }
}
