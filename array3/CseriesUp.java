/* Given n>=0, create an array with the pattern {1,    1, 2,    1, 2, 3,   ... 1, 2, 3 .. n}
 * (spaces added to show the grouping). Note that the length of the array will be 1 + 2 + 3
 * ... + n, which is known to sum to exactly n*(n + 1)/2.
*/

package array3;
import java.util.Arrays;

public class CseriesUp {
    public int[] seriesUp(int n) {
	int l = (n*(n+1))/2;
	int[] arr = new int[l];
	int s = 0;
	int k = 0;
	for (int i = 1; i <= n; ++i) {
	    k = 1;
	    for (int j = s; j < s+i; ++j) {
		arr[j] = k;
		++k;
	    }
	    s = s + i;
	}
	return arr;
    }
}
