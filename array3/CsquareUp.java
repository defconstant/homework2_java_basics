/* Given n>=0, create an array length n*n with the following pattern, shown here for n=3
 * : {0, 0, 1,    0, 2, 1,    3, 2, 1} (spaces added to show the 3 groups).
*/

package array3;

public class CsquareUp {
    public int[] squareUp(int n) {
	int l = n*n;
        int[] arr = new int[l];
        int k = 0;
        for (int i = 1; i <= n; ++i) {
            k = 1;
            for (int j = n*i-1; j >= n*(i-1); --j) {
                if (k <= i)
                    arr[j] = k;
                else
                    arr[j] = 0;
                ++k;
            }
        }
        return arr;
    }
}
