import java.util.Arrays;
import recursion1.*;
import recursion2.*;
import logic1.*;
import array3.*;
import string3.*;

public class Main {
    public static void main(String[] args) {
	//Recursion-1 > array11
	int[] nums_array11 = {1, 11, 3, 11, 11};
	System.out.println(new Carray11().array11(nums_array11, 0));
	//Recursion-1 > bunnyEars2
	System.out.println(new CbunnyEars2().bunnyEars2(10));
	//Recursion-1 > powerN
	System.out.println(new CpowerN().powerN(10, 3));
	//Logic-1 > cigarParty
	System.out.println(new CcigarParty().cigarParty(39, true));
	//Recursion-2 > groupSum6
	int[] nums_groupsum6 = {5, 6, 2};
	System.out.println(new CgroupSum6().groupSum6(0, nums_groupsum6, 8));
	//Array-3 > seriesUp
	System.out.println(Arrays.toString(new CseriesUp().seriesUp(4)));
	//Array-3 > squareUp
	System.out.println(Arrays.toString(new CsquareUp().squareUp(4)));
	//String-3 > countTriple
	System.out.println(new CcountTriple().countTriple("abYYYabXXXXXab"));
    }
}
